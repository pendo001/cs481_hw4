import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {


    Widget titleSection = Container(
      padding: const EdgeInsets.all(20), //creates padding around the outside
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment(0.0,0.0),
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Ghostface',
                    style: TextStyle (
                      color: Colors.white,
                    fontWeight: FontWeight.bold,
                      fontSize: 30,
                  ),
                  ),
                    ),
                Container(
                  alignment: Alignment(0.0,0.0),
                  child: Text('Scream',
                    style: TextStyle (
                      fontStyle: FontStyle.italic,
                      color: Colors.pinkAccent,
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ); //Title Section

    Widget infoSection = Container(
      alignment: Alignment(0.0,0.0),
      child: Row(
        children: [
          FavoriteWidget(),
        ],
      ),
    );

    Widget infoSection2 = Container(
      alignment: Alignment(0.0,0.0),
      child: Row(
        children: [
          FavoriteWidget2(),
        ],
      ),
    );

    Widget titleSection2 = Container(
      padding: const EdgeInsets.all(20), //creates padding around the outside
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment(0.0,0.0),
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Michael Myers',
                    style: TextStyle (
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 30,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment(0.0,0.0),
                  child: Text('Halloween',
                    style: TextStyle (
                      fontStyle: FontStyle.italic,
                      color: Colors.pinkAccent,
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ); //Title Section2

    Widget buttonSection = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(Colors.pink, Icons.arrow_back, 'Back'),
          _buildButtonColumn(Colors.white, Icons.home, 'Home'),
          _buildButtonColumn(Colors.pink, Icons.arrow_forward, 'Next'),
        ],
      ),
    );//Button Section

    Widget textSection = Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text.rich(TextSpan(children: [
            TextSpan(text: 'Name:', style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white,)),
            TextSpan(text: ' Ghostface', style: TextStyle(color: Colors.white,),)
          ])),
          Text.rich(TextSpan(children: [
            TextSpan(text: 'First Appearance:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white,)),
            TextSpan(text: ' Scream (1996)', style: TextStyle(color: Colors.white,),)
          ])),
          Text.rich(TextSpan(children: [
            TextSpan(text: 'Last Appearance:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white,)),
            TextSpan(text: ' Scream (2022)', style: TextStyle(color: Colors.white,),)
          ])),
          Text.rich(TextSpan(children: [
            TextSpan(text: 'Bio:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white,)),
            TextSpan(text:  '  Ghostface (alternatively stylized as Ghost Face or GhostFace) is a fictional identity adopted by several characters in the Scream series.'
                            'The figure is primarily mute in person but voiced over the phone by Roger L. Jackson, regardless of who is behind the mask.',
              style: TextStyle(color: Colors.white,),
            )
          ])),
        ],
      ),
    );//Text Section

    Widget textSection2 = Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text.rich(TextSpan(children: [
            TextSpan(text: 'Name:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, ) ),
            TextSpan(text: ' Michael Myers', style: TextStyle(color: Colors.white,),)
          ])),
          Text.rich(TextSpan(children: [
            TextSpan(text: 'First Appearance:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white,)),
            TextSpan(text: ' Halloween (1978)', style: TextStyle(color: Colors.white,),)
          ])),
          Text.rich(TextSpan(children: [
            TextSpan(text: 'Last Appearance:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white,)),
            TextSpan(text: ' Halloween Ends (2022)', style: TextStyle(color: Colors.white,),)
          ])),
          Text.rich(TextSpan(children: [
            TextSpan(text: 'Bio:', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white,)),
            TextSpan(text:  ' Michael Myers is a fictional character from the Halloween series of slasher films. He first appears in 1978 in John Carpenters Halloween as a young boy who murders his elder sister, Judith Myers, then fifteen years later, returns home to Haddonfield to murder more teenagers.'
                'In the original Halloween, the adult Michael Myers, referred to as The Shape in the closing credits, was portrayed by Nick Castle for most of the film, with Tony Moran and Tommy Lee Wallace substituting in the final scenes. ',
              style: TextStyle(color: Colors.white,),
            )
          ])),
        ],
      ),
    );//Text Section


    return MaterialApp(
      title: 'Homework 4',
      home: Scaffold(
        backgroundColor: Colors.black26,
        appBar: AppBar(
          backgroundColor: Colors.pinkAccent,
          title: Text('Homework 4'),
        ),
        body: ListView(
            children: [
              LogoApp(),
              titleSection,
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.pinkAccent[200],
                    width: 10.0,
                  )
                ),
                child: Image.asset('images/Ghostface.jpg',
                  width: 200,
                  height: 400,
                  fit: BoxFit.cover,
                )
              ), //GHOST FACE IMAGE
              infoSection,
              textSection,
              titleSection2,
              Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.pinkAccent[200],
                        width: 10.0,
                      )
                  ),
                  child: Image.asset('images/michaelmyers.jpg',
                    width: 200,
                    height: 400,
                    fit: BoxFit.cover,
                  )
              ), //MICHAEL MYERS IMAGE
              infoSection2,
              textSection2,
              buttonSection,
            ]
        ),
      ),

    );

  } //BUILD End Bracket

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
                label,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: color,
                )
            )
        ),
      ],
    );
  } //Button Struct

} //MY APP

class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.3, end: 1);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          child: titleAni(),
        ),
      ),
    );
  }
}

class titleAni extends StatefulWidget {
  _titleAniState createState() => _titleAniState();
}

class _titleAniState extends State<titleAni> {

  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(4.0),
        child: Text('Scary Movie Slashers', style: TextStyle(color: Colors.pink, fontWeight: FontWeight.bold, fontSize: 40,) ),
      )
    );
  }

}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    controller.forward();
  }



  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
//
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavored = false;
  int _favoriteCount = 401;
  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerRight,
            icon: (_isFavored ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.pink,
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
            width: 30,
            child: Container(
              child: Text('$_favoriteCount', style: TextStyle(color: Colors.white,)),
            )
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: _isFavored ? Text('Image Liked', style: TextStyle(color: Colors.pink),) : null,
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isFavored) {
        _favoriteCount -= 1;
        _isFavored = false;
      }
      else {
        _favoriteCount += 1;
        _isFavored = true;
      }
    });
  }
}

class FavoriteWidget2 extends StatefulWidget {
  @override
  _FavoriteWidgetState2 createState() => _FavoriteWidgetState2();
}

class _FavoriteWidgetState2 extends State<FavoriteWidget2> {
  bool _isFavored = false;
  int _favoriteCount = 205;
  @override
  Widget build(BuildContext context){
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerRight,
            icon: (_isFavored ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.pink,
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
            width: 30,
            child: Container(
              child: Text('$_favoriteCount', style: TextStyle(color: Colors.white,)),
            )
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: _isFavored ? Text('Image Liked', style: TextStyle(color: Colors.pink),) : null,
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if(_isFavored) {
        _favoriteCount -= 1;
        _isFavored = false;
      }
      else {
        _favoriteCount += 1;
        _isFavored = true;
      }
    });
  }
}
